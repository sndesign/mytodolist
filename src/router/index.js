import Vue from 'vue'
import store from '../store'

import Router from 'vue-router'
import Login from '@/components/login/Login'
import Forgot from '@/components/forgot/Forgot'
import Dashboard from '@/components/dashboard/Dashboard'
import Jobs from '@/components/jobs/Jobs'
import Job from '@/components/job/Job'
import Accept from '@/components/accept/Accept'

import Addons from '@/components/addons/Addons'
import AddonEdit from '@/components/addons/AddonEdit'

import Users from '@/components/users/Users'
import UserShow from '@/components/users/UserShow'
import UserEdit from '@/components/users/UserEdit'

Vue.use(Router)

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isLoggedIn) {
    next()
    return
  }
  next('/dashboard')
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/forgot-password',
      name: 'Forgot Password',
      component: Forgot,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/accept-invitation',
      name: 'Accept Invitation',
      component: Accept,
      props: { reset: false },
      beforeEnter: ifAuthenticated
    },
    {
      path: '/reset-password',
      name: 'Reset Password',
      component: Accept,
      props: { reset: true },
      beforeEnter: ifAuthenticated
    },
    // {
    //   path: '/customers',
    //   name: 'Customers',
    //   component: Customers
    // },
    {
      path: '/customers',
      name: 'Customers',
      component: Users,
      props: { userType: 'customer' }
    },
    {
      path: '/customers/:id',
      name: 'Customer',
      component: UserShow,
      props: { userType: 'customer', id: true }
    },
    {
      path: '/customer/new',
      name: 'New Customer',
      component: UserEdit,
      props: { userType: 'customer', newUser: true }
    },
    // {
    //   path: '/gurus',
    //   name: 'Gurus',
    //   component: User,
    //   props: { userType: 'guru' }
    // },
    {
      path: '/customers/:id/edit',
      name: 'Edit Customer',
      component: UserEdit,
      props: { userType: 'customer' }
    },
    {
      path: '/gurus',
      name: 'Gurus',
      component: Users,
      props: { userType: 'guru' }
    },
    {
      path: '/gurus/:id',
      name: 'Guru',
      component: UserShow,
      props: { userType: 'guru' }
    },
    {
      path: '/gurus/:id/edit',
      name: 'Edit Guru',
      component: UserEdit,
      props: { userType: 'guru' }
    },
    {
      path: '/guru/new',
      name: 'New Guru',
      component: UserEdit,
      props: { userType: 'guru', newUser: true }
    },
    {
      path: '/my-service-schedule',
      name: 'Service Schedule',
      component: UserShow,
      props: { userType: 'service' }
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/jobs',
      name: 'Jobs',
      component: Jobs
    },
    {
      path: '/jobs/:id',
      name: 'Job',
      component: Job,
      props: true
    },
    {
      path: '/addons',
      name: 'Add Ons',
      component: Addons
    },
    {
      path: '/addon/new',
      name: 'Add Ons New',
      component: AddonEdit,
      props: { newAddOn: true }
    },
    {
      path: '/addons/:id/edit',
      name: 'Add Ons Edit',
      component: AddonEdit
    }
  ]
})
