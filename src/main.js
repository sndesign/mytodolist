// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from './backend/vue-axios'
import store from './store'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import VuejsDialog from 'vuejs-dialog'

// import 'jquery'
// import 'bootstrap'

Vue.config.productionTip = false
Vue.use(VueMoment, {
  moment
})
Vue.config.devtools = true

router.beforeEach((to, from, next) => {
  if (!store.getters.isLoggedIn) {
    if (to.path !== '/' && to.path !== '/forgot-password' && to.path !== '/accept-invitation' && to.path !== '/reset-password') {
      next('/')
    } else {
      next()
    }
  } else {
    next()
  }
})

Vue.use(VuejsDialog)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  store,
  template: '<App/>',
  components: { App }
})
