export function isLoggedIn (state) {
  return state => !!state.token
}

export function apiError (state) {
  let error = state.apiError
  if (!error) {
    return false
  } else {
    if (error.response) {
      return 'Something went wrong, sorry, please refresh and try again'
    } else if (error.request) {
      return 'No response from server, please refresh and try again'
    } else {
      return error.message
    }
  }
}
export function isFetching (state) {
  return state.isFetching
}

export function users (state) {
  if (Array.isArray(state.users)) {
    return state.users
  } else {
    return []
  }
}

export function currentUser (state) {
  return state.currentUser
}

export function role (state) {
  return state.currentUser.role
}

export function customers (state) {
  if (Array.isArray(state.users)) {
    return state.users.filter(user => user.role === 'customer')
  } else {
    return []
  }
}

export function gurus (state) {
  if (Array.isArray(state.users)) {
    return state.users.filter(user => user.role === 'guru')
  } else {
    return []
  }
}

export function jobs (state) {
  if (Array.isArray(state.jobs)) {
    return state.jobs.filter(job => {
      var jobDate = new Date(job.date)
      jobDate.setHours(0, 0, 0, 0)
      var today = new Date()
      today.setHours(0, 0, 0, 0)
      if (today <= jobDate) {
        return true
      } else {
        return false
      }
    })
  } else {
    return []
  }
}

export function jobsPast (state) {
  if (Array.isArray(state.jobs)) {
    return state.jobs.filter(job => {
      var jobDate = new Date(job.date)
      jobDate.setHours(0, 0, 0, 0)
      var today = new Date()
      today.setHours(0, 0, 0, 0)
      if (jobDate < today) {
        return true
      } else {
        return false
      }
    })
  } else {
    return []
  }
}

export function allJobs (state) {
  // if (Array.isArray(state.jobsPast)) {
  //   console.log('state.jobsPast')
  //   console.log(state.jobsPast)
  //   var allJobs = state.jobsPast.concat(state.jobs)
  //   return allJobs
  // } else {
  if (Array.isArray(state.jobs)) {
    return state.jobs
  } else {
    return []
  }
  // }
}

export function jobById (state, id) {
  if (Array.isArray(state.jobs)) {
    return state.jobs.filter(job => job.id === id)
  } else {
    return []
  }
}

export function pending (state) {
  return state.pending
}

export function uploading (state) {
  return state.uploading
}

export function addons (state) {
  return state.addOns
}

export function templates (state) {
  if (Array.isArray(state.templates)) {
    return state.templates
  } else {
    return []
  }
}

export function getToken () {
  console.log(sessionStorage.token)
  return sessionStorage.token
}

export function getPageTitle (state) {
  return state.currentPage
}

export function errorState (state) {
  return state.error
}
