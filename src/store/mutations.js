export function API_FAILURE (state, data) {
  state.apiError = data
  state.isFetching = false
}

export function SET_FETCHING (state, data) {
  state.apiError = false
  state.isFetching = data
}

export function SET_JOBS (state, data) {
  state.jobs = data.data
}

export function SET_TEMPLATES (state, data) {
  state.templates = data.data
}

export function NEW_TEMPLATE (state, data) {
  state.templates.push(data.data)
}

export function DELETE_TEMPLATE (state, data) {
  var templateIndex = state.templates.findIndex(t => Number(t.id) === Number(data))
  state.templates.splice(templateIndex, 1)
}

export function DELETE_JOB (state, data) {
  var jobIndex = state.jobs.findIndex(j => Number(j.id) === Number(data))
  state.jobs.splice(jobIndex, 1)
}

export function SET_USERS (state, data) {
  // console.log(data)
  state.users = data.data
}

export function UPDATE_USER (state, data) {
  // console.log(data)
  var index = state.users.findIndex(u => u.id === data.data.id)
  state.users[index] = data.data
}

export function CREATE_USER (state, data) {
  state.users.push(data.data)
}

export function SET_PAST_JOBS (state, data) {
  state.jobsPast = data.data
}

export function SET_ADD_ONS (state, data) {
  state.addOns = data.data
}

export function SET_PAGE_TITLE (state, data) {
  state.currentPage = data
}

export function SET_TOKEN (state, data) {
  state.userToken = data
}

export function SET_CURRENT_USER (state, data) {
  state.currentUser = data
}

export function LOGIN (state) {
  state.pending = true
}

export function LOGIN_RESET (state) {
  state.pending = false
}

export function LOGIN_SUCCESS (state, token) {
  state.pending = false
  state.isLoggedIn = true
  state.error.status = false
  state.token = token
}

export function NOT_PENDING (state) {
  state.pending = false
}

export function LOGIN_ERROR (state, data) {
  state.error = data
  state.isLoggedIn = false
  state.token = ''
}

export function UPLOADING (state) {
  state.apiError = false
  state.uploading = true
}

export function UPLOADED (state) {
  state.uploading = false
}

export function LOGOUT (state) {
  state.isLoggedIn = false
  state.token = ''
}

export function UPDATE_TASK (state, data) {
  if (data.parent_type === 'Template') {
    for (var prop in data.data) {
      state.templates.find(j => j.id === data.jobId).ordered_tasks.find(t => t.id === data.id)[prop] = data.data[prop]
    }
  } else {
    for (var jobProp in data.data) {
      state.jobs.find(j => j.id === data.jobId).ordered_tasks.find(t => t.id === data.id)[jobProp] = data.data[jobProp]
    }
  }
}

export function UPDATE_JOB (state, data) {
  for (var prop in data.data) {
    state.jobs.find(j => j.id === data.jobId)[prop] = data.data[prop]
  }
}

export function UPDATE_TEMPLATE (state, data) {
  for (var prop in data.data) {
    state.templates.find(j => j.id === data.data.id)[prop] = data.data[prop]
  }
}

export function CREATE_TASK (state, data) {
  if (data.parent_type === 'Template') {
    state.templates.find(j => j.id === data.parent_id).ordered_tasks.push(data)
  } else {
    state.jobs.find(j => j.id === data.parent_id).ordered_tasks.push(data)
  }
}

export function DELETE_TASK (state, data) {
  if (data.parent_type === 'Template') {
    var deleteIndex = state.templates.find(j => j.id === data.parentId).ordered_tasks.findIndex(t => t.id === data.id)
    state.templates.find(j => j.id === data.parentId).ordered_tasks.splice(deleteIndex, 1)
  } else {
    var deleteIndexJob = state.jobs.find(j => j.id === data.parentId).ordered_tasks.findIndex(t => t.id === data.id)
    state.jobs.find(j => j.id === data.parentId).ordered_tasks.splice(deleteIndexJob, 1)
  }
}

export function DELETE_ADDON (state, data) {
  var index = state.addOns.findIndex(ao => ao.id === data)
  state.addOns.splice(index, 1)
}

export function UPDATE_ADDON (state, data) {
  // console.log(data)
  for (var prop in data.data) {
    state.addOns.find(ao => ao.id === data.data.id)[prop] = data.data[prop]
  }
}

export function CREATE_ADDON (state, data) {
  state.addOns.push(data.data)
}
