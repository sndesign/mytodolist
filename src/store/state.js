export function isLoggedIn () {
  return !!localStorage.getItem('token')
}

export function apiError () {
  return false
}

export function isFetching () {
  return false
}

export function uploading () {
  return false
}

export function todaysDate () {
  return new Date()
}

export function currentPage () {
  return ''
}

export function jobs () {
  return []
}

export function addOns () {
  return []
}

export function users () {
  return []
}

export function templates () {
  return []
}

export function jobsPast () {
  return []
}

export function error () {
  return {
    status: false,
    reason: ''
  }
}

export function token () {
  return localStorage.getItem('token') || ''
}
