import axios from '../backend/vue-axios/axios'
import router from '../router/index'
// import { rejects } from 'assert';
// import store from './index'

// Get data
// Jobs

export function getJobs (context) {
  return new Promise((resolve, reject) => {
    context.commit('SET_FETCHING', true)
    axios.get('/jobs', { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      context.commit('SET_JOBS', response)
      context.commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      context.commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function updateJob ({ commit }, jobData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.patch('/jobs/' + jobData.jobId, jobData.data)
      .then(() => {
        commit('UPDATE_JOB', jobData)
        commit('SET_FETCHING', false)
        resolve()
      })
      .catch((error) => {
        commit('API_FAILURE', error)
        reject(error)
        console.log(error)
      })
  })
}

export function newTemplate ({ commit }, templateData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.post('/templates/', templateData)
      .then((response) => {
        commit('NEW_TEMPLATE', response)
        commit('SET_FETCHING', false)
        resolve()
      })
      .catch((error) => {
        commit('API_FAILURE', error)
        reject(error)
      })
  })
}

export function updateTemplate ({ commit }, templateData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.patch('/templates/' + templateData.templateId, templateData.data)
      .then((response) => {
        commit('UPDATE_TEMPLATE', response)
        commit('SET_FETCHING', false)
        resolve()
      })
      .catch((error) => {
        commit('API_FAILURE', error)
        reject(error)
      })
  })
}

// Tasks
//
export function newTask ({ commit }, taskData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.post('/tasks', taskData, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('CREATE_TASK', response.data)
      commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function updateTask ({ commit }, taskData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.patch('/tasks/' + taskData.id, taskData.data, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('UPDATE_TASK', taskData)
      commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function deleteTask ({ commit }, taskData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.delete('/tasks/' + taskData.id, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('DELETE_TASK', taskData)
      commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function setTaskPosition ({ commit }, taskData) {
  var taskPositions = {}
  for (var index = 0; index < taskData.data.length; index++) {
    var taskId = taskData.data.item(index).getAttribute('data-task-id')
    var jobId = taskData.data.item(index).getAttribute('data-job-id')
    taskPositions[index] = { 'position': index + 1, 'taskId': taskId, 'jobId': jobId }
    commit('UPDATE_TASK', { parent_type: taskData.parent_type, 'id': Number(taskId), 'jobId': Number(jobId), data: { position: index } })
  }
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.put('/tasks/set-positions', {'taskPositions': taskPositions}, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

// Add Ons
//
export function getAddOns (context) {
  return new Promise((resolve, reject) => {
    context.commit('SET_FETCHING', true)
    axios.get('/addons', { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      context.commit('SET_ADD_ONS', response)
      context.commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      context.commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function editAddOn ({ commit }, addOnData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.put('/addons/' + addOnData.id, addOnData.data, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('UPDATE_ADDON', response)
      commit('SET_FETCHING', false)
      resolve()
      router.push('/addons')
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function createAddOn ({ commit }, addOnData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.post('/addons/', addOnData.data, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('CREATE_ADDON', response)
      commit('SET_FETCHING', false)
      resolve()
      router.push('/addons')
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function deleteAddOn ({ commit }, id) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.delete('/addons/' + id, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('DELETE_ADDON', id)
      commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

// Users
//
export function getUsers (context) {
  return new Promise((resolve, reject) => {
    context.commit('SET_FETCHING', true)
    axios.get('/users', { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      context.commit('SET_USERS', response)
      context.commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      context.commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function editUsers ({ commit, dispatch }, userData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)

    axios.patch('/users/' + userData.id, userData.data, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('UPDATE_USER', response)
      commit('SET_FETCHING', false)
      resolve()

      if (userData.data.user.role === 'customer') {
        router.push('/customers')
      } else {
        router.push('/gurus')
      }
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function uploadUserPhoto ({ commit }, userData) {
  return new Promise((resolve, reject) => {
    commit('UPLOADING')
    axios.patch('/users/' + userData.id, userData.data, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      resolve()
      commit('UPLOADED')
    })
    .catch((error) => {
      commit('UPLOADED')
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function createUser ({ commit, dispatch }, userData) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)

    axios.post('/users/', userData.data, { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      commit('CREATE_USER', response)
      commit('SET_FETCHING', false)
      resolve()
      if (userData.data.user.role === 'customer') {
        router.push('/customers')
      } else {
        router.push('/gurus')
      }
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

// Templates
//
export function getTemplates (context) {
  return new Promise((resolve, reject) => {
    context.commit('SET_FETCHING', true)
    axios.get('/templates', { headers: {
      'Content-Type': 'application/json'
    } })
    .then((response) => {
      context.commit('SET_TEMPLATES', response)
      context.commit('SET_FETCHING', false)
      resolve()
    })
    .catch((error) => {
      context.commit('API_FAILURE', error)
      reject(error)
    })
  })
    // .catch((error) => context.commit('API_FAILURE', error))
}

export function rejobifyTemplates ({dispatch, commit}, data) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.put('/templates/' + data.id + '/rejobify', { headers: {
      'Content-Type': 'application/json'
    } })
    .then(() => {
      commit('SET_FETCHING', false)
      dispatch('getJobs')
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function deleteTemplates ({dispatch, commit}, data) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.delete('/templates/' + data.id, { headers: {
      'Content-Type': 'application/json'
    } })
    .then(() => {
      commit('DELETE_TEMPLATE', data.id)
      commit('SET_FETCHING', false)
      dispatch('getJobs')
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function deleteJob ({dispatch, commit}, data) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.delete('/jobs/' + data.id, { headers: {
      'Content-Type': 'application/json'
    } })
    .then(() => {
      commit('DELETE_JOB', data.id)
      commit('SET_FETCHING', false)
      dispatch('getJobs')
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

// Tasks for authentication
//
export function login ({ commit, dispatch, context }, creds) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.post('/authentication', creds)
      .then(request => {
        if (!request.data.auth_token) {
          let error = new Error('Authentication has failed')
          commit('API_FAILURE', error)
          reject(error)
        } else {
          localStorage.setItem('token', request.data['auth_token'])
          axios.defaults.headers.common['Authorization'] = request.data['auth_token']

          commit('LOGIN_SUCCESS', request.data['auth_token'])
          commit('SET_CURRENT_USER', request.data.user)

          commit('SET_FETCHING', false)

          resolve()
        }
      }).catch(error => {
        commit('SET_FETCHING', false)
        localStorage.removeItem('token')
        if (error.message === 'Request failed with status code 401') {
          dispatch('loginFail', 'Your email or password is incorrect.')
        } else if (error.message === 'Network Error') {
          dispatch('loginFail', 'There was a connection error, check you have an internet connection.')
        } else {
          dispatch('loginFail', 'Oops, something went wrong, try refreshing the page and retrying.')
        }
        reject(error)
      })
  })
}

export function reset ({ commit, dispatch }, creds) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.post('/invitation/reset?email=' + creds.email.toLowerCase())
    .then(request => {
      commit('SET_FETCHING', false)
      commit('LOGIN_SUCCESS')
      resolve()
    })
    .catch((error) => {
      commit('API_FAILURE', error)
      reject(error)
    })
  })
}

export function accept ({ commit, dispatch }, creds) {
  return new Promise((resolve, reject) => {
    commit('SET_FETCHING', true)
    axios.put('/invitation/accept?token=' + creds.token + '&password=' + creds.password)
    .then(request => {
      commit('LOGIN_SUCCESS')
      commit('SET_FETCHING', false)
      resolve()
    })
  }).catch(error => {
    commit('SET_FETCHING', false)
    if (error.message === 'Request failed with status code 401') {
      dispatch('loginFail', 'Your reset token has expired, try resetting your password again.')
    } else if (error.message === 'Network Error') {
      dispatch('loginFail', 'There was a connection error, check you have an internet connection.')
    } else {
      dispatch('loginFail', 'Oops, something went wrong, try refreshing the page and retrying.')
    }
  })
}

export function logout ({ commit }) {
  localStorage.removeItem('token')
  commit('SET_CURRENT_USER', '')
  commit('LOGOUT')
}

export function loginFail ({ commit }, reason) {
  // commit('SET_TOKEN', '')
  localStorage.removeItem('token')
  commit('LOGIN_ERROR', {state: true, reason: reason})
  commit('SET_CURRENT_USER', '')
  commit('LOGOUT')
  commit('LOGIN_RESET')
}
