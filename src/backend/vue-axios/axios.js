// Default Settings AXIOS

import axios from 'axios'

const API_URL = process.env.API_URL

export default axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json'
  }
})

const token = localStorage.getItem('token')

if (token) {
  axios.defaults.headers.common['Authorization'] = token
}

//
// export const upload = axios.create({
//   baseURL: API_URL,
//   headers: {
//     'Content-Type': 'multipart/form-data',
//     'Authorization': store.getters.getToken
//   }
// })
